const repositories: {
  name: string;
  subtitle?: string;
  slug: string;
  logo: string;
  logoDark?: string;
  hidden?: boolean;
}[] = [
  {
    name: "Windows",
    slug: "windows",
    logo: "/icons/windows.svg"
  },
  {
    name: "Arch Linux",
    slug: "arch",
    logo: "/icons/archlinux.svg"
  },
  {
    name: "Debian based",
    subtitle: "(Debian, Ubuntu, Mint, etc.)",
    slug: "debian",
    logo: "/icons/debian.svg"
  },
  {
    name: "Gentoo",
    slug: "gentoo",
    logo: "/icons/gentoo.svg"
  },
  {
    name: "Fedora",
    slug: "fedora",
    logo: "/icons/fedora.svg"
  },
  {
    name: "openSUSE",
    subtitle: "(Leap, Tumbleweed, Slowroll)",
    slug: "opensuse",
    logo: "/icons/opensuse.svg",
    logoDark: "/icons/opensuse.svg"
  },
  {
    name: "Other Linux",
    slug: "linux",
    logo: "/icons/tux.png"
  },
  {
    name: "macOS",
    slug: "macos",
    logo: "/icons/apple.svg",
    logoDark: "/icons/apple-white.svg"
  },
  
  {
    name: "OpenBSD",
    subtitle: "(deprecated)",
    slug: "openbsd",
    logo: "/icons/openbsd.png",
    hidden: true
  }
];

export default repositories;
